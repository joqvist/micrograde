#!/usr/bin/python3
import os, sys
from datetime import datetime
from os import path
import zipfile
import subprocess
import shutil

if __name__ == "__main__":
    logs = []
    for log in os.listdir("logs"):
        if not log.endswith(".err"):
            tm = path.getmtime(path.join("logs", log))
            logs += [(tm, log)]
    logs = sorted(logs, key=lambda x: x[0], reverse=True)
    ts_prefix = "Test suite: "
    print("Checking JARs...")
    for ts, ID in logs:
        ts = datetime.fromtimestamp(ts)
        res = 0
        with open(path.join("logs", ID)) as f:
            lines = [ln.strip() for ln in f.readlines()]
            for ln in lines:
                if ln.endswith("...OK"):
                    res += 1
            if len(lines) >= 3 and lines[-3] == "ALL CLEAR":
                res = "OK"
        if res != "OK":
            continue
        print(f'UPLOAD ID {ID}  {ts.strftime("%Y-%m-%d %H:%M:%S")}')
        try:
            with zipfile.ZipFile(path.join("uploads", ID, "compiler.jar")) as zf:
                zf.extract("src/parser/parser.beaver", "temp")
                shutil.copyfile("temp/src/parser/parser.beaver", f'temp/src/{ID}.beaver')
                proc = subprocess.run([
                    'java', '-cp', 'nbfront.jar', 'beaver.comp.run.Make',
                    '-d', "temp/gen", '-t', '-c', '-w', 'temp/src/parser/parser.beaver'], 
                    stdout=subprocess.DEVNULL,
                    stderr=subprocess.PIPE,
                    universal_newlines=True)
                print(proc.stderr)
        except zipfile.BadZipFile:
            print("Bad zipfile!")
