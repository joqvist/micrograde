#!/usr/bin/python3
# Requires the Python library "watchdog".
# python3 -m pip install watchdog
import os, sys
import time
import datetime
import subprocess
import json
from threading import Thread
from os.path import abspath

from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

# Allowed values for the test suite parameter:
all_suites = set()
with open('grader_static/assignments.json') as fp:
    for suite in json.load(fp):
        all_suites.add(suite['Id'])

def find_files(token):
    jar = f"uploads/{token}/compiler.jar"
    conf = f"uploads/{token}/conf"
    if not os.path.isfile(jar):
        return (False, "Jar file not found.")
    elif not os.path.isfile(conf):
        return (False, "Config file not found.")
    else:
        return (True, (jar, conf))

logfile = lambda token: f"logs/{token}"

def grade(token, jar, suite, log, err):
    if not suite in all_suites:
        err.write("Error: unknown test suite: %s\n" % suite)
        log.write("Error: unknown test suite\n")
        return
    log.write(f"Test suite: {suite}\n")
    log.flush()
    try:
        cmd =[
            abspath("grader/grader"),
            abspath(f"uploads/{token}"),
            abspath(f"testcases/target/{suite}")
        ]
        p = subprocess.Popen(cmd, cwd=abspath("grader"),
                stdin=open("/dev/null", "rb"),
                shell=False, stderr=err, stdout=log)
        p.wait()
    except Exception as e:
        err.write(f"{e}\n")
        sys.stderr.write(f"{e}\n")
        log.write("Internal error\n")

def docker_thread(token):
    with open(f"{logfile(token)}.err", "w") as err:
        with open(logfile(token), "w") as log:
            found, res = find_files(token)
            if found:
                jar, conf = res
                with open(conf, "r") as config:
                    suite = config.readline().strip()
                    grade(token, jar, suite, log, err)
            else:
                err.write(f"{res}\n")
                log.write(f"Error: {res}\n")
            log.write(f"EOF:{token}\n")

def get_suite(token):
    try:
        found, res = find_files(token)
        if found:
            jar, conf = res
            with open(conf, "r") as config:
                suite = config.readline().strip()
                if suite in all_suites:
                    return suite
    except:
        pass
    return "?"

def handle_item(path):
    os.remove(path)
    token = path.split('/')[-1]
    print(datetime.datetime.now().strftime("%Y-%d-%m %H:%M:%S"), token, get_suite(token))
    sys.stdout.flush() # Flush output for systemd journaling.
    thread = Thread(target=docker_thread, args=[token])
    thread.start()

class EventHandler(FileSystemEventHandler):
    def __init__(self):
        FileSystemEventHandler.__init__(self)

    def on_created(self, event):
        if event.is_directory:
            # Skip directories
            return
        handle_item(event.src_path)


if __name__ == "__main__":
    if not os.path.isdir("uploads/queue"):
        os.makedirs("uploads/queue")
    for existing in os.listdir("uploads/queue"):
        handle_item("uploads/queue/" + existing)

    handler = EventHandler()
    observer = Observer()
    observer.schedule(handler, "uploads/queue", recursive=False)
    observer.start()
    try:
        while True:
            time.sleep(10)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
