#!/usr/bin/python3
# Parses logs and writes to unique_jars a list of all submissions that had unique jar files.
# Arguments - [1] uploads directory
import os, sys
from datetime import datetime
from os import path
import hashlib

def sha1sum(fn):
    sha1 = hashlib.sha1()
    with open(fn, 'rb') as f:
        while True:
            data = f.read(4096)
            if not data:
                break
            sha1.update(data)
    return sha1.hexdigest()

if __name__ == "__main__":
    logs = []
    for log in os.listdir("logs"):
        if not log.endswith(".err"):
            tm = path.getmtime(path.join("logs", log))
            logs += [(tm, log)]
    logs = sorted(logs, key=lambda x: x[0], reverse=True)
    ts_prefix = "Test suite: "
    passed = {}
    runs = {}
    suites = set()
    groups = 8
    tab = []
    jars = []
    jar_hashes = set()
    combo = set()
    uploads = sys.argv[1] if len(sys.argv) > 1 else 'uploads'
    for ts, log in logs:
        ts = datetime.fromtimestamp(ts)
        suite = "?"
        res = 0
        with open(path.join("logs", log)) as f:
            lines = [ln.strip() for ln in f.readlines()]
            for ln in lines:
                if ln.startswith(ts_prefix):
                    suite = ln[len(ts_prefix):]
                elif ln.endswith("...OK"):
                    res += 1
            if len(lines) >= 3 and lines[-3] == "ALL CLEAR":
                res = "OK"
        suites.add(suite)
        if res == "OK":
            passed[suite] = passed.get(suite, 0) + 1
        runs[suite] = runs.get(suite, 0) + 1
        sha1 = sha1sum(f"{uploads}/{log}/compiler.jar")
        if not sha1 in jar_hashes:
            jars += [log]
            jar_hashes.add((sha1))
        dup = (sha1, suite) in combo
        combo.add((sha1, suite))
        tab.append((log, ts, suite, res, sha1, dup))
    print('''id,suite,result,sha1,dup''')
    for log, ts, suite, res, sha1, dup in tab:
        print(f'{log},{suite},{res},{sha1},{dup}')

with open("unique_jars", "w") as f:
    for jar in jars:
        f.write(f"{jar}\n")
