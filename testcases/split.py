#!/usr/bin/python3
import os
import sys
from sys import stdin
import argparse
import re
skip_comments = False
comment = re.compile('^--.*$')
head = re.compile('^\[(.+)\]$')
expect = re.compile('^\[expect\]$')

parser = argparse.ArgumentParser()
parser.add_argument('out_dir', type=str, help='output directory')
args = parser.parse_args()

if not os.path.isdir(args.out_dir):
    os.mkdir(args.out_dir)

def write_expected(tc, name, kind, lines, i):
    fn = os.path.join(args.out_dir, f"{str(tc).zfill(3)}_{name}_{kind[0]}.expected")
    print(fn)
    with open(fn,'w') as f:
        while i < len(lines):
            if head.match(lines[i]):
                break
            f.write(lines[i])
            i += 1
    return i

def write_test(tc, name, kind, lines, i):
    fn = os.path.join(args.out_dir, f"{str(tc).zfill(3)}_{name}_{kind[0]}.in")
    print(fn)
    with open(fn,'w') as f:
        while i < len(lines):
            if not skip_comments or not comment.match(lines[i]):
              if head.match(lines[i]):
                  break
              f.write(lines[i])
            i += 1
    if i < len(lines) and expect.match(lines[i]):
        i = write_expected(tc, name, kind, lines, i+1)
    return i

lines = stdin.readlines()
tc = i = 0
while i < len(lines):
    m = head.match(lines[i])
    if m:
        name, kind = m.group(1).split(':')
        i = write_test(tc, name, kind, lines, i+1)
        tc += 1
    else:
        i += 1
