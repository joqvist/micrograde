#!/bin/bash
if [ ! -d "target" ]; then
  mkdir "target"
fi
for conf in config/*.conf; do
  name=$(basename $conf)
  name=${name%\.conf}
  odir="target/$name"
  if [ -d $odir ]; then
    rm -r $odir
  fi
  source $conf
  mkdir $odir
  IFS="," read -ra SUITES <<< "$SUITE"
  echo "${SUITES[@]}"
  cat ${SUITES[@]} | ./split.py $odir/testcases
  cp $RUN_TEST $odir/run_test
  cp $CHECK_RESULT $odir/check_result
done
