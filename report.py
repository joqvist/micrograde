#!/usr/bin/python3
import os, sys
from datetime import datetime
from os import path
import hashlib

if __name__ == "__main__":
    logs = []
    for log in os.listdir("logs"):
        if not log.endswith(".err"):
            tm = path.getmtime(path.join("logs", log))
            logs += [(tm, log)]
    logs = sorted(logs, key=lambda x: x[0], reverse=True)
    ts_prefix = "Test suite: "
    print(\
'''<html>
<head>
  <title>Test Report</title>
  <link rel="stylesheet" type="text/css" href="/grader.css"/>
</head>
<body>
''')
    passed = {}
    runs = {}
    suites = set()
    groups = 8
    tab = []
    for ts, log in logs:
        ts = datetime.fromtimestamp(ts)
        suite = "?"
        res = 0
        with open(path.join("logs", log)) as f:
            lines = [ln.strip() for ln in f.readlines()]
            for ln in lines:
                if ln.startswith(ts_prefix):
                    suite = ln[len(ts_prefix):]
                elif ln.endswith("...OK"):
                    res += 1
            if len(lines) >= 3 and lines[-3] == "ALL CLEAR":
                res = "OK"
        suites.add(suite)
        if res == "OK":
            passed[suite] = passed.get(suite, 0) + 1
        runs[suite] = runs.get(suite, 0) + 1
        tab.append((log, ts, suite, res))
    print('</table><table>')
    print('  <tr><th>suite</th><th>runs</th><th>passed</th><th>pass&#37;</th></tr>')
    suites = sorted(suites)
    suite_id = dict(map(lambda x: (x[1],x[0]), enumerate(suites)))
    for i,suite in enumerate(suites):
        r, p = runs.get(suite, 0), passed.get(suite, 0)
        if r > 0:
            group = f"group{i%groups}"
            print(f'  <tr><td><span class="{group}">{suite}</span></td><td>{r}</td><td>{p}</td><td>{"%.1f"%((100.0*p)/r)}</td></tr>')
    print('''<table>
<tr><th>Submission</th><th>Timestamp</th><th>Test Suite</th><th>Result (#pass)</th><th>SHA1</th></tr>''')
    b = bytearray(128*1024)
    mv = memoryview(b)
    for log, ts, suite, res in tab:
        if res == "OK":
            res = '<span class="green">OK</span>'
        group = f"group{suite_id[suite]%groups}"
        with open(f'uploads/{log}/compiler.jar', 'rb', buffering=0) as f:
            h = hashlib.sha1()
            for n in iter(lambda: f.readinto(mv), 0):
                h.update(mv[:n])
        sha1 = h.hexdigest()
        print(f'<tr><td><a href="http://yolo.cs.lth.se/result/{log}">{log}</a></td><td>{ts.strftime("%Y-%m-%d %H:%M:%S")}</td><td><span class="{group}">{suite}</span></td><td>{res}</td><td>{sha1}</td></tr>')
    print('''</table></body>
</html>''')
