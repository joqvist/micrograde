FROM golang:1.19

WORKDIR /go/src/app

COPY src .

RUN go build .

COPY grader_static data
COPY site data

CMD ["./micrograde"]
