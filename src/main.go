package main

import (
	"net/http"
	"log"
	"io"
	"strconv"
	"fmt"
	"os"
	"crypto/sha1"
	"mime"
	"sync"
	"time"
	"html/template"
	"path"
	"path/filepath"
	"github.com/golang-commonmark/markdown"
	"encoding/json"
)

const (
	RESULT_REQUEST = "/result/"
	LOG_REQUEST = "/log/"
	TOKEN_EXP_REQUEST = "/token_expired/"
	ISSUED = iota
	USED = iota
)

type Grader struct {
	sync.Mutex
	tokens map[string]int
}

type Assignment struct {
	Id string
	Name string
	Instructions template.HTML
}

type AssignmentTemplateData struct {
	Assignment Assignment
}

type SubmitTemplateData struct {
	Token string
	Assignments []Assignment
}

func renderMarkdown(w http.ResponseWriter, filename string, path string) {
	md := markdown.New()
	text, _ := os.ReadFile(path)
	fmt.Fprintf(w, `<html lang=en>
<head>
<title>%s</title>
<meta charset="UTF-8">
<style type="text/css">
p code {
  background-color: #F8F8F8;
  border: 1px solid #CCC;
  border-radius: 3px;
  padding: 0px 5px;
  white-space: nowrap;
}
pre {
  background-color: #F8F8F8;
  border: 1px solid #CCC;
  border-radius: 3px;
  padding: 6px 10px;
  overflow: auto;
  width: 800px;
}</style>
</head>
<body><div id="content">%s</div></body>
</html>`, filename, md.RenderToString(text))
}

func renderPage(w http.ResponseWriter, r *http.Request) {
	filename := r.URL.Path[len("/"):]
	path := "data/" + filename
	log.Printf("Request: %s\n", path)
	if path == "data/" {
		renderMarkdown(w, "Index", "data/index.md")
	} else if _, err := os.Stat(path); err == nil {
		w.Header().Set("Content-Type", mime.TypeByExtension(filepath.Ext(path)))
		data, _ := os.ReadFile(path)
		w.Write(data)
	} else if _, err := os.Stat(path + ".md"); err == nil {
		renderMarkdown(w, filename, path + ".md")
	} else {
		http.Error(w, "File not found", http.StatusNotFound)
	}
}

func GenToken() string {
	hash := sha1.New()
	io.WriteString(hash, "inte hemligt")
	io.WriteString(hash, strconv.FormatInt(time.Now().Unix(), 10))
	return fmt.Sprintf("%x", hash.Sum(nil))[:8]
}

func (gg *Grader) NextToken() string {
	gg.Lock()
	token := GenToken()
	log.Printf("NextToken: %s\n", token)
	for ; true;  {
		if _, ok := gg.tokens[token]; !ok {
			gg.tokens[token] = ISSUED
			// Check that output directory does not already exist.
			path := "uploads/" + token
			if _, err := os.Stat(path); err != nil {
				break
			}
		}
		token = GenToken()
		log.Printf("NextToken[retry]: %s\n", token)
	}
	gg.Unlock()
	return token
}

func (gg *Grader) assignHandler(w http.ResponseWriter, r *http.Request, assn Assignment) {
	if r.Method == "GET" {
		t, err := template.ParseFiles("assignment.template")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		t.Execute(w, AssignmentTemplateData{
			Assignment: assn,
		})
	} else {
		r.ParseMultipartForm(1 << 20) // Max size = 1MB.
		token := gg.NextToken()
		assignment := r.Form.Get("assignment")
		file, handler, err := r.FormFile("uploadfile")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		log.Printf("Submission %s: %s\n", token, handler.Filename)
		defer file.Close()
		http.Redirect(w, r, RESULT_REQUEST + token, 301)
		if err := os.MkdirAll("./uploads/" + token, os.ModeDir | os.ModePerm); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		jarName := fmt.Sprintf("uploads/%s/compiler.jar", token)
		confName := fmt.Sprintf("uploads/%s/conf", token)
		dest, err := os.OpenFile(jarName, os.O_WRONLY | os.O_CREATE, 0666)
		log.Printf("Writing: %s\n", jarName)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		defer dest.Close()
		io.Copy(dest, file)
		conf, err := os.OpenFile(confName, os.O_WRONLY | os.O_CREATE, 0666)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		defer conf.Close()
		fmt.Fprintf(conf, "%s\n", assignment)
		fmt.Fprintf(conf, "%s", r.Form.Get("arguments"))
		log.Printf("Writing: %s\n", confName)
		defer dest.Close()
		io.Copy(dest, file)
		// Add token to work queue.
		err = os.MkdirAll("./uploads/queue", os.ModeDir | os.ModePerm)
		if err != nil {
			log.Printf("%s\n", err.Error())
			return
		}
		_, err = os.OpenFile("./uploads/queue/" + token, os.O_RDONLY | os.O_CREATE, 0666)
		if err != nil {
			log.Printf("%s\n", err.Error())
			return
		}
	}
}

func (gg *Grader) submitHandler(w http.ResponseWriter, r *http.Request) {
	t, err := template.ParseFiles("submit.template")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	t.Execute(w, SubmitTemplateData{
		Token: "unused",
		Assignments: assignments,
	})
}

func resultHandler(w http.ResponseWriter, r *http.Request) {
	token := path.Base(r.URL.Path)
	t, err := template.ParseFiles("result.template")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	t.Execute(w, token)
}

func tokenExpiredHandler(w http.ResponseWriter, r *http.Request) {
	token := path.Base(r.URL.Path)
	t, err := template.ParseFiles("token_expired.template")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	t.Execute(w, token)
}

func logHandler(w http.ResponseWriter, r *http.Request) {
	token := path.Base(r.URL.Path)
	log.Printf("Log query: %s\n", token)
	path := "logs/" + token
	if _, err := os.Stat(path); err == nil {
		text, _ := os.ReadFile(path)
		w.Write(text)
	} else {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("Not found."))
		log.Printf("Log %s not found.\n", token)
	}
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

var assignments []Assignment
func main() {
	http.HandleFunc("/", renderPage)
	grader := Grader{tokens: map[string]int{}}
	assndat, err := os.ReadFile("data/assignments.json")
	check(err)
	err = json.Unmarshal(assndat, &assignments)
	check(err)
	for _, a := range assignments {
		assn := a
		log.Printf("Found assignment: %s\n", assn.Id)
		instr, err := os.ReadFile("data/assignment/" + assn.Id + ".md")
		check(err)
		md := markdown.New()
		assn.Instructions = template.HTML(md.RenderToString(instr))
		http.HandleFunc("/submit/" + assn.Id, func(w http.ResponseWriter, r *http.Request) {
			grader.assignHandler(w, r, assn)
		})
	}
	http.HandleFunc("/submit", grader.submitHandler)
	http.HandleFunc(RESULT_REQUEST, resultHandler)
	http.HandleFunc(LOG_REQUEST, logHandler)
	http.HandleFunc(TOKEN_EXP_REQUEST, tokenExpiredHandler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
