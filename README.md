# MicroGrade - Minimalistic Assignment Grader

MicroGrade requires the following tools/libraries:
git, Docker, Bash, Python3, Python3 watchdog library.

## Setup

A configuration file named `config` needs to be created in the base MicroGrade root
directory. The file needs to look like this:

    SITE_REPO=<SITE URL>
    DOCKER_NAME=web # OPTIONAL
    WEB_PORT=8080 # OPTIONAL


`SITE_REPO` is optional, if set it should point to a git repository containing static
web content for the grader.

Run the `./get-repos.sh` script to fetch the configured repos or to update them if they have new
changes.

## Running

The web server and automatic grader can be started with the script `./service/start.sh`.
However, this method of starting the grader should only be used for testing.

The preferred method of running the grader is by installing a systemd service
to keep the grader running in the background and allow it to start on system
boot (in case of power failure, for example). See instructions below.

## Install Systemd Service

To install the systemd service, run the script `service/install-systemd.sh`

To see the live logs from the systemd service, use the command `journalctl -fu grader`

To see the logs for the webserver run `docker logs web`

### Reporter Service

An optional service to generate test reports can be installed by running `service/install-reporter.sh`

## Update Grader

To rebuild and restart the grader, run `./get-repos.sh` and then `./service/start-container.sh`
