#!/usr/bin/python3
# Re-grade all submissions.
import os, sys
import datetime
from watch import *

if __name__ == "__main__":
    submissions = [ f for f in os.listdir("uploads") if len(f) == 8 ]
    N = len(submissions)
    for i, token in enumerate(submissions):
        print(f'{i+1}/{N} {token} {get_suite(token)}')
        sys.stdout.flush() # Flush output for systemd journaling.
        docker_thread(token)
