The compiler outputs assembly code for the input program.

### Input

The path to the input file is given as a command-line argument to the compiler.
The input to the program is passed on the standard input stream.

### Output

The compiler exit code is non-zero if compilation failed.

If compilation succeeds, the compiler prints the assembly code for the compiled program to
the standard output stream.

The program exit code should always be zero.

The following commands are used to compile and run the compiler output:

    java -jar compiler.jar source > ans.s
    as --gstabs ans.s -o ans.o
    ld ans.o -o answer
    ./answer < input > output
    diff -qw output expected
