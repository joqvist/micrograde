The compiler performs simple name analysis on functions and variables.

### Input

The path to the input file is given as a command-line argument to the compiler.

### Output

The compiler exit code is non-zero if any syntax or name analysis error was found.
