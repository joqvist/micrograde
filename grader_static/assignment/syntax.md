The compiler analyses the syntax of SimpliC programs.

### Input

The path to the input file is given as a command-line argument to the compiler.

### Output

The compiler exit code is non-zero if a syntax error was found.
