The compiler checks that the input program is semantically correct.

### Input

The path to the input file is given as a command-line argument to the compiler.

### Output

The compiler exit code is non-zero if any syntax or semantic error was found.

