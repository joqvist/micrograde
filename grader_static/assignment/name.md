The compiler checks that all function and variable names are used correctly in the input program.

### Input

The path to the input file is given as a command-line argument to the compiler.

### Output

The compiler exit code is non-zero if any syntax or name analysis error was found.
