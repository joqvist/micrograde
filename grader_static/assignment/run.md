The compiler interprets the input program.

### Input

The path to the input file is given as a command-line argument to the compiler.
The input to the interpreted program is passed on the standard input stream.

### Output

The output of the interpreted program is printed by the compiler to the standard output stream.

The compiler exit code is non-zero if compilation failed, or if there was any error interpreting
the program.

The following commands are used grade your compiler:

    java -jar compiler.jar source < input > output
    diff -qw output expected
