#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR/..
./report.py > site/report.html
docker cp site/report.html web:/go/src/app/data
