#!/bin/bash
# Installs the grader reporting service.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
USER=$(whoami)
DEST=${1:-/etc/systemd/system}
echo "Installing systemd unit: $DEST/grader-reporter.service"
sudo tee $DEST/grader-reporter.service > /dev/null <<EOF
[Unit]
Description=MicroGrade Report Generator
Requires=grader.service

[Service]
User=$USER
Group=$USER
ExecStart=$DIR/update-report.sh
WorkingDirectory=$DIR

[Install]
WantedBy=multi-user.target
EOF
echo "Installing systemd unit: $DEST/grader-reporter.timer"
sudo tee $DEST/grader-reporter.timer > /dev/null <<EOF
[Unit]
Description=Timer for MicroGrade reporter
Requires=grader-reporter.service

[Timer]
Unit=grader-reporter.service
OnUnitActiveSec=10m
RandomizedDelaySec=1m
AccuracySec=1s

[Install]
WantedBy=timers.target
EOF
sudo systemctl daemon-reload
sudo systemctl enable grader-reporter.timer
sudo systemctl start grader-reporter
