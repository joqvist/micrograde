#!/bin/bash
set -eu
USER=$1 # The user which the grader will run as.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
/bin/su -l $USER -c "/usr/bin/tmux new-session -s grader -d $DIR/boot.sh \; set -t grader remain-on-exit on"
