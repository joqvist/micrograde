#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
set -e
cd $DIR/..
source config
DOCKER_NAME=${DOCKER_NAME:-web}
WEB_PORT=${WEB_PORT:-80}
(cd ./grader; ./build.sh)
(cd ./testcases; ./build.sh)
mkdir -p uploads/queue
mkdir -p logs
docker build -t micrograde .
set +e
docker stop $DOCKER_NAME
set -e
docker run -it \
  --rm \
  --detach \
  --user $(id -u):$(id -g) \
  --publish ${WEB_PORT}:8080 \
  --name $DOCKER_NAME \
  -v $PWD/uploads:/go/src/app/uploads \
  -v $PWD/logs:/go/src/app/logs \
  micrograde
