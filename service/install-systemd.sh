#!/bin/bash
# Installs the grader as a systemd service in /etc/systemd/system
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
USER=$(whoami)
UNIT=${1:-/etc/systemd/system/grader.service}
echo "Installing systemd unit: $UNIT"
sudo tee $UNIT > /dev/null <<EOF
[Unit]
Description=MicroGrade Service
After=docker.service
Requires=docker.service

[Service]
User=$USER
Group=$USER
ExecStart=$DIR/start.sh
WorkingDirectory=$DIR

[Install]
WantedBy=multi-user.target
EOF
sudo systemctl daemon-reload
sudo systemctl start grader
sudo systemctl enable grader
