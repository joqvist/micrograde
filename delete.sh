#!/bin/bash
# Deletes a single submission by ID(=$1).

set -eu
rm -r uploads/$1
rm logs/$1
rm logs/$1.err
