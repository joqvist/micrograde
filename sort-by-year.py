#!/usr/bin/python3
# Move submissions to archive directory based on year.
import os, sys, time
from datetime import datetime
from os import path

if __name__ == "__main__":
    logs = []
    years = set()
    for log in os.listdir("logs"):
        if not log.endswith(".err"):
            tm = path.getmtime(path.join("logs", log))
            logs += [(tm, log)]
            years.add(datetime.fromtimestamp(tm).year)
    current_year = datetime.fromtimestamp(time.time()).year
    for year in years:
        if year == current_year:
            continue
        if not os.path.isdir(f'archive/{year}/uploads'):
            os.makedirs(f'archive/{year}/uploads')
        if not os.path.isdir(f'archive/{year}/logs'):
            os.makedirs(f'archive/{year}/logs')

    print("Moving files...")
    for ts, ID in logs:
        year = datetime.fromtimestamp(ts).year
        if year == current_year:
            continue
        print(f'Move {ID} -> archive/{year}')
        os.rename(f'uploads/{ID}', f'archive/{year}/uploads/{ID}')
        os.rename(f'logs/{ID}', f'archive/{year}/logs/{ID}')
        log_err = f'logs/{ID}.err'
        if os.path.getsize(log_err) > 0:
            os.path.remove(log_err)
        else:
            os.rename(log_err, f'archive/{year}/logs/{ID}.err')
    print("Done!")
