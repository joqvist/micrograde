#!/bin/bash
if [ ! -e config ]; then
  echo "ERROR: config file does not exist! (See README for instructions)"
  exit 1
fi
source config
if [ ! -z "$SITE_REPO" ]; then
  echo "cloning site from $SITE_REPO"
  if [ ! -d site ]; then
    git clone $SITE_REPO site
  else
    (cd site; git pull)
  fi
else
  mkdir -p site
fi
