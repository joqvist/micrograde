#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <sys/wait.h>

#ifndef TIMEOUT
#define TIMEOUT (3)
#endif
#define EXIT_TIMEOUT (66)
#define EXIT_ERROR (1)

static void on_alarm(int signo);

static pid_t fork_pid;
static int timedout = 0;

int main(int argc, char** argv)
{
  if (argc < 2) {
    printf("Missing command argument.\n");
    printf("Usage: timeout COMMAND [ARG...]\n");
    return EXIT_ERROR;
  }

  fork_pid = fork();
  if (fork_pid < 0) {
    perror("fork error");
    return EXIT_ERROR;
  }

  if (fork_pid == 0) {
    // Child process.
    execv(argv[1], argv+1);
    perror("execv error");
    return EXIT_ERROR;
  } else {
    // Parent process.
    int status;
    signal(SIGALRM, on_alarm);
    alarm(TIMEOUT);
    while (1) {
      pid_t wpid = wait(&status);
      if (wpid == -1) {
        // Interrupted.
        return EXIT_ERROR;
      } else {
        if (WIFEXITED(status)) {
          // Exited.
          return WEXITSTATUS(status);
        } else if (!WIFCONTINUED(status)) {
          // Signalled.
          return timedout ? EXIT_TIMEOUT : EXIT_ERROR;
        }
      }
    }
  }
  return 0;
}

static void on_alarm(int signo)
{
  kill(fork_pid, SIGKILL);
  timedout = 1;
}
